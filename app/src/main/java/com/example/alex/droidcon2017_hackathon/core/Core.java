package com.example.alex.droidcon2017_hackathon.core;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.alex.droidcon2017_hackathon.io.Dress;
import com.example.alex.droidcon2017_hackathon.io.DressSamples;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.HashMap;

/**
 * Created by alex on 08/04/2017.
 */

public class Core {
    private static final String TAG = "core";

    private static Core mInstance = null;

    private HashMap<String, Dress> wishList;
    private HashMap<String, Dress> wardrobe;
    private Dress tmpDress;
    private Context mCtx;

    private Core(Context context) {
        mCtx = context;
        Fresco.initialize(mCtx);
        wardrobe = DressSamples.WARDROBE_DATABASE;
        wishList = DressSamples.FAVOURITES_DATABASE;
    }

    public static synchronized Core getInstance(){
        return getInstance(null);
    }

    public static synchronized Core getInstance(@Nullable Context context) {
        if (null == mInstance)
            mInstance = new Core(context);
        return mInstance;
    }

    public boolean addToWishList(Dress dress){
        if(!wishList.containsKey(dress.getId())){
            wishList.put(dress.getId(), dress);
            return true;
        }
        return false;
    }

    public boolean addTOWardrobe(Dress dress){
        if(!wardrobe.containsKey(dress.getId())){
            wardrobe.put(dress.getId(), dress);
            if(wishList.containsKey(dress.getId())) //remove from wishList if present
                wishList.remove(dress.getId());
            return true;
        }
        return false;
    }

    public HashMap<String, Dress> getWishList() {
        Log.d(TAG, "wishlist size: " + wishList.size());
        return wishList;
    }

    public HashMap<String, Dress> getWardrobe() {
        Log.d(TAG, "wardrobe size: " + wardrobe.size());
        return wardrobe;
    }

    public Dress getTmpDress() {
        return tmpDress;
    }

    public void setTmpDress(Dress tmpDress) {
        this.tmpDress = tmpDress;
    }

    public static boolean checkMultiplePermissions(Activity activity, String[] permissionsRequest) {
        boolean flag = true;
        final int permissionsRequest_length = permissionsRequest.length;// Moved  permissionsRequest.length call out of the loop to local variable permissionsRequest_length
        for (int i = 0; i < permissionsRequest_length; i++) {
            if (ContextCompat.checkSelfPermission(activity, permissionsRequest[i]) != PackageManager.PERMISSION_GRANTED)
                flag = false;
        }
        return flag;
    }



}
