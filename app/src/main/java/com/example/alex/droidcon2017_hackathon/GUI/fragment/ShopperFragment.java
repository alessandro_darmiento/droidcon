package com.example.alex.droidcon2017_hackathon.GUI.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.alex.droidcon2017_hackathon.GUI.adapter.ChatMessageAdapter;
import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.core.Core;
import com.example.alex.droidcon2017_hackathon.core.GlobalVariables;
import com.example.alex.droidcon2017_hackathon.io.ChatMessage;
import com.example.alex.droidcon2017_hackathon.io.Dress;
import com.example.alex.droidcon2017_hackathon.io.DressSamples;
import com.facebook.drawee.view.SimpleDraweeView;

import org.alicebot.ab.AIMLProcessor;
import org.alicebot.ab.Bot;
import org.alicebot.ab.Chat;
import org.alicebot.ab.Graphmaster;
import org.alicebot.ab.MagicBooleans;
import org.alicebot.ab.MagicStrings;
import org.alicebot.ab.PCAIMLProcessorExtension;
import org.alicebot.ab.Timer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;

/**
 * Created by alex on 08/04/2017.
 */

public class ShopperFragment extends Fragment {

    public final static String TAG = "shopper";

    private ListView mListView;
    private LinearLayout subjectView;
    private FloatingActionButton mButtonSend;
    private EditText mEditTextMessage;
    private ChatMessageAdapter mAdapter;
    private SimpleDraweeView image;

    public Bot bot;
    public static Chat chat;

    public Dress dress;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.shopper_fragment, container, false);
        mListView = (ListView) rootView.findViewById(R.id.listView);
        mButtonSend = (FloatingActionButton) rootView.findViewById(R.id.btn_send);
        mEditTextMessage = (EditText) rootView.findViewById(R.id.et_message);
        subjectView = (LinearLayout) rootView.findViewById(R.id.shopper_subject);
        image = (SimpleDraweeView)rootView.findViewById(R.id.subject_image);
        mAdapter = new ChatMessageAdapter(getContext(), new ArrayList<ChatMessage>());
        mListView.setAdapter(mAdapter);

        //code for sending the message
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mEditTextMessage.getText().toString();
                //bot
                String response = chat.multisentenceRespond(mEditTextMessage.getText().toString());
                if (TextUtils.isEmpty(message)) {
                    return;
                }
                sendMessage(message, false);
                mimicOtherMessage(response);
                mEditTextMessage.setText("");
                mListView.setSelection(mAdapter.getCount() - 1);
            }
        });

        //checking SD card availablility
        boolean a = isSDCARDAvailable();
        Log.d(TAG, "IsSDCARDAvailable: " + a);
        //receiving the assets from the app directory
        AssetManager assets = getResources().getAssets();
        //File file = new File(Environment.getExternalStoragePublicDirectory("Pixide"),"");
        File jayDir = new File(Environment.getExternalStoragePublicDirectory("hari/bots/Vinx"), "");
        boolean b = jayDir.mkdirs();
        Log.d(TAG, "Dir created: " + b + " file "+ jayDir.exists()+ " and "+ jayDir.isDirectory());
        if (jayDir.exists()) {
            //Reading the file
            try {
                for (String dir : assets.list("Vinx")) {
                    Log.d(TAG, "Dir "+ dir);
                    File subdir = new File(jayDir.getPath() + "/" + dir);
                    boolean subdir_check = subdir.mkdirs();
                    for (String file : assets.list("Vinx/" + dir)) {
                        File f = new File(jayDir.getPath() + "/" + dir + "/" + file);
                        if (f.exists()) {
                            continue;
                        }
                        InputStream in = null;
                        OutputStream out = null;
                        in = assets.open("Vinx/" + dir + "/" + file);
                        out = new FileOutputStream(jayDir.getPath() + "/" + dir + "/" + file);
                        //copy file from assets to the mobile's SD card or any secondary memory
                        copyFile(in, out);
                        in.close();
                        in = null;
                        out.flush();
                        out.close();
                        out = null;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//get the working directory
        MagicStrings.root_path = Environment.getExternalStorageDirectory().toString() + "/hari";
        System.out.println("Working Directory = " + MagicStrings.root_path);
        AIMLProcessor.extension =  new PCAIMLProcessorExtension();
//Assign the AIML files to bot for processing
        bot = new Bot("Vinx", MagicStrings.root_path, "chat");
        chat = new Chat(bot);
        String[] args = null;
        mainFunction(args);

        fillViews();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter(GlobalVariables.GOTO_ASSISTANT));
        if(Core.getInstance().getTmpDress()!=null){
            String message = Core.getInstance().getTmpDress().getId();
            String response = chat.multisentenceRespond(Core.getInstance().getTmpDress().getId());
            if (!TextUtils.isEmpty(message)) {
                sendMessage(message, true);
                mimicOtherMessage(response);
            }
        }
        return rootView;
    }
    private void sendMessage(String message, boolean image) {
        ChatMessage chatMessage = new ChatMessage(message, true, false);
        Log.d(TAG, "mando ora ora msg");
        mAdapter.isImage(image);
        mAdapter.add(chatMessage);
        //respond as Helloworld
        //mimicOtherMessage("HelloWorld");
    }

    private void mimicOtherMessage(String message) {
        ChatMessage chatMessage = new ChatMessage(message, false, false);
        mAdapter.add(chatMessage);
    }

    private void sendMessage() {
        ChatMessage chatMessage = new ChatMessage(null, true, true);
        mAdapter.add(chatMessage);

        mimicOtherMessage();
    }

    private void mimicOtherMessage() {
        ChatMessage chatMessage = new ChatMessage(null, false, true);
        mAdapter.add(chatMessage);
    }


    //check SD card availability
    public static boolean isSDCARDAvailable(){
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)? true :false;
    }
    //copying the file
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
    //Request and response of user and the bot
    public static void mainFunction (String[] args) {
        MagicBooleans.trace_mode = false;
        System.out.println("trace mode = " + MagicBooleans.trace_mode);
        Graphmaster.enableShortCuts = true;
        Timer timer = new Timer();
        String request = "Hello.";
        String response = chat.multisentenceRespond(request);

        System.out.println("Human: "+request);
        System.out.println("Robot: " + response);
    }

    private void fillViews(){
        if((dress = Core.getInstance().getTmpDress())!=null){
            subjectView.setVisibility(View.GONE);
            image.setImageURI(dress.getUrlImage());
        } else {
            subjectView.setVisibility(View.GONE);
        }

    }

    private String nfcID;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Broadcast arrivato");
            if (intent.getAction().equals(GlobalVariables.GOTO_ASSISTANT)) {
                nfcID = intent.getExtras().getString("nfcID");
                Log.d(TAG, "MAndo msg");
                sendMessage(nfcID, true);
            }
        }

    };

}