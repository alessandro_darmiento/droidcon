package com.example.alex.droidcon2017_hackathon.GUI.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alex.droidcon2017_hackathon.GUI.adapter.CabinetItemAdapter;
import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.core.Core;
import com.example.alex.droidcon2017_hackathon.core.GlobalVariables;
import com.example.alex.droidcon2017_hackathon.io.Dress;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by alex on 09/04/2017.
 */

public class CabinetItemFragment extends Fragment {
    public static final String TAG = "cabFrag";

    private RecyclerView recycle;
    private ArrayList<Dress> dresses;
    private CabinetItemAdapter adapter;
    private SwipeRefreshLayout refreshLayout;

    public CabinetItemFragment(){
    }

    public void setList(ArrayList<Dress> dresses){
        this.dresses = dresses;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG,"Create cabinet fragment");
        View rootView = inflater.inflate(R.layout.wardrobe_fragment, container, false);
        recycle = (RecyclerView) rootView.findViewById(R.id.wardrobe_recycler);
        refreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.wardrobe_refresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
            }
        });
        adapter = new CabinetItemAdapter(dresses);
        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recycle.setAdapter(adapter);

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(receiver, new IntentFilter(GlobalVariables.NEW_ITEM_READY));
        return rootView;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<Dress> newContent = new ArrayList<>();
            switch(intent.getExtras().getString("sender")){
                case "wishlist":
                    newContent = new ArrayList<>(Core.getInstance().getWishList().values());
                    break;
                case "wardrobe":
                    newContent = new ArrayList<>(Core.getInstance().getWardrobe().values());
                    break;
            }
            for(Dress d: newContent){
                if(!dresses.contains(d)){
                    dresses.add(d);
                    adapter.notifyDataSetChanged();
                }
            }
        }

    };
}
