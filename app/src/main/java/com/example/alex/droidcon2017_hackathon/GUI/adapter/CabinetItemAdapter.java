package com.example.alex.droidcon2017_hackathon.GUI.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.io.Dress;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Flavia on 09/04/2017.
 */

public class CabinetItemAdapter extends RecyclerView.Adapter<CabinetItemAdapter.ViewHolder> {

    private static final String TAG = "cabinetItemAdaper";
    private final ArrayList<Dress> cabinetDresses;

    public CabinetItemAdapter(ArrayList<Dress> cabinetDresses){
        Log.d(TAG, "Adapter created");
        Log.d(TAG, "appena fatto "+ cabinetDresses.size());
        this.cabinetDresses = cabinetDresses;
    }

    @Override
    public CabinetItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.d(TAG, "On create view holder");
        View myView= LayoutInflater.from(parent.getContext()).inflate(R.layout.cabinet_item, parent,false);
        return new ViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(CabinetItemAdapter.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder");
        holder.name.setText(cabinetDresses.get(position).getName());
        holder.properties.setText(cabinetDresses.get(position).getTags().toString());
        holder.image.setImageURI(Uri.parse(cabinetDresses.get(position).getUrlImage()));
    }

    @Override
    public int getItemCount() {

        Log.d(TAG, "get count "+ cabinetDresses.size());
        return cabinetDresses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView image;
        private final TextView name;
        private final TextView properties;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.cabinet_image);
            name = (TextView) itemView.findViewById(R.id.cabinet_name);
            properties = (TextView) itemView.findViewById(R.id.cabinet_properties);
        }
    }
}
