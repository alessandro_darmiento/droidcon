package com.example.alex.droidcon2017_hackathon.GUI.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alex.droidcon2017_hackathon.GUI.adapter.CabinetItemAdapter;
import com.example.alex.droidcon2017_hackathon.GUI.adapter.CabinetMenuAdapter;
import com.example.alex.droidcon2017_hackathon.GUI.adapter.MenuAdapter;
import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.io.Dress;

import java.util.ArrayList;

/**
 * Created by alex on 08/04/2017.
 */

public class CabinetFragment extends Fragment {

    private RecyclerView recycle;
    private ArrayList<Dress> dresses;

    private ViewPager mPager;
    private TabLayout tabLayout;
    private CabinetMenuAdapter adapter;

    public CabinetFragment(){
    }

    public void setList(ArrayList<Dress> dresses){
        this.dresses = dresses;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.cabinet_fragment, container, false);

        mPager = (ViewPager) rootView.findViewById(R.id.cabinet_viewpager);
        adapter = new CabinetMenuAdapter(getFragmentManager());
        mPager.setAdapter(adapter);

        tabLayout = (TabLayout) rootView.findViewById(R.id.cabinet_tablayout);
        tabLayout.setupWithViewPager(mPager);
        tabLayout.setTabTextColors(0xFF000000, 0xFF000000);

        tabLayout.getTabAt(0).setText("MY WARDROBE");
        tabLayout.getTabAt(1).setText("WHISHLIST");


        return rootView;
    }
}
