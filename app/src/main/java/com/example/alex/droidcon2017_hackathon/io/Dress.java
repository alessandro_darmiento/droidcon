package com.example.alex.droidcon2017_hackathon.io;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alex on 08/04/2017.
 */

public class Dress {
    private String id;
    private String mainColor;
    private int taglia;
    private String urlImage;
    private ArrayList<String> tags;
    private String name;

    public Dress(String id, String mainColor, int taglia, String urlImage,  ArrayList<String> tags, String name){
        this.id = id;
        this.mainColor = mainColor;
        this.tags = tags;
        this.taglia = taglia;
        this.urlImage = urlImage;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getMainColor() {
        return mainColor;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public int getTaglia() {
        return taglia;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getName() {
        return name;
    }
}
