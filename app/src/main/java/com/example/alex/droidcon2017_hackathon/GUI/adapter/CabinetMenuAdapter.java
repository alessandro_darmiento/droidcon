package com.example.alex.droidcon2017_hackathon.GUI.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.alex.droidcon2017_hackathon.GUI.fragment.CabinetItemFragment;
import com.example.alex.droidcon2017_hackathon.core.Core;
import com.example.alex.droidcon2017_hackathon.io.Dress;

import java.util.ArrayList;

/**
 * Created by alex on 09/04/2017.
 */

public class CabinetMenuAdapter extends FragmentPagerAdapter {
    private ArrayList<Dress> listValues;


    public CabinetMenuAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        CabinetItemFragment fragment = new CabinetItemFragment();
        switch (position){
            case 0:
                listValues = new ArrayList<>(Core.getInstance().getWardrobe().values());
                break;
            case 1:
                listValues = new ArrayList<>(Core.getInstance().getWishList().values());
                break;

        }
        fragment.setList(listValues);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
