package com.example.alex.droidcon2017_hackathon.GUI.adapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.alex.droidcon2017_hackathon.GUI.fragment.CabinetFragment;
import com.example.alex.droidcon2017_hackathon.GUI.fragment.ScanFragment;
import com.example.alex.droidcon2017_hackathon.GUI.fragment.ShopperFragment;

/**
 * Created by alex on 08/04/2017.
 */

public class MenuAdapter extends FragmentPagerAdapter {

    public MenuAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ScanFragment();
            case 1:
                CabinetFragment frag = new CabinetFragment();
                //TODO frag.setList();
                return frag;
            case 2:
                return new ShopperFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        // return null to display only the icon
        return null;
    }
}
