package com.example.alex.droidcon2017_hackathon.GUI.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.core.Core;
import com.example.alex.droidcon2017_hackathon.io.ChatMessage;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * Created by alex on 08/04/2017.
 */

public class ChatMessageAdapter extends ArrayAdapter<ChatMessage> {
    private static final int MY_MESSAGE = 0, OTHER_MESSAGE = 1, MY_IMAGE = 2, OTHER_IMAGE = 3;
    private boolean image;

    public ChatMessageAdapter(Context context, List<ChatMessage> data) {
        super(context, R.layout.item_mine_message, data);
    }
    @Override
    public int getViewTypeCount() {
        // my message, other message, my image, other image
        return 4;
    }
    @Override
    public int getItemViewType(int position) {
        ChatMessage item = getItem(position);
        if (item.isMine() && !item.isImage()) return MY_MESSAGE;
        else if (!item.isMine() && !item.isImage()) return OTHER_MESSAGE;
        else if (item.isMine() && item.isImage()) return MY_IMAGE;
        else return OTHER_IMAGE;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        if (viewType == MY_MESSAGE) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_message, parent, false);
            LinearLayout laymsg = (LinearLayout) convertView.findViewById(R.id.mine_msg_lay);
            LinearLayout layimg = (LinearLayout) convertView.findViewById(R.id.mine_photo_lay);
            if(!getItem(position).getContent().contains("057")) {
                laymsg.setVisibility(View.VISIBLE);
                layimg.setVisibility(View.GONE);
                TextView textView = (TextView) convertView.findViewById(R.id.text);
                textView.setText(getItem(position).getContent());
            }else{
                laymsg.setVisibility(View.GONE);
                layimg.setVisibility(View.VISIBLE);
                SimpleDraweeView img = (SimpleDraweeView) convertView.findViewById(R.id.mine_photo);
                img.setImageURI(Core.getInstance().getTmpDress().getUrlImage());
            }
        } else if (viewType == OTHER_MESSAGE) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_message, parent, false);
            LinearLayout laymsg = (LinearLayout) convertView.findViewById(R.id.other_msg_lay);
            LinearLayout layimg = (LinearLayout) convertView.findViewById(R.id.other_photo_lay);
            if(!getItem(position).getContent().contains(":")) {
                laymsg.setVisibility(View.VISIBLE);
                layimg.setVisibility(View.GONE);
                TextView textView = (TextView) convertView.findViewById(R.id.text);
                textView.setText(getItem(position).getContent());
            }else{
                laymsg.setVisibility(View.GONE);
                layimg.setVisibility(View.VISIBLE);
                SimpleDraweeView img = (SimpleDraweeView) convertView.findViewById(R.id.other_photo);
                img.setImageURI(getItem(position).getContent());
            }
        } else if (viewType == MY_IMAGE) {
            //convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_image, parent, false);
        } else {
            // convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_image, parent, false);
        }
        convertView.findViewById(R.id.chatMessageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "onClick", Toast.LENGTH_LONG).show();
            }
        });
        return convertView;
    }

    public void isImage(boolean image) {
        this.image = image;
    }
}