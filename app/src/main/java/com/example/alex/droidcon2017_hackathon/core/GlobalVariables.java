package com.example.alex.droidcon2017_hackathon.core;

import com.example.alex.droidcon2017_hackathon.io.Dress;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alex on 08/04/2017.
 */
public class GlobalVariables {


    public final static String NFC_READY = "nfc_ready";
    public static final String GOTO_CABINET = "goto_cabinet";
    public static final String GOTO_FAVOURITE = "goto_favourite";
    public static final String GOTO_ASSISTANT = "goto_assistent";
    public static final String NEW_ITEM_READY = "new_item_ready";

}
