package com.example.alex.droidcon2017_hackathon.GUI.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alex.droidcon2017_hackathon.GUI.activity.MainActivity;
import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.core.Core;
import com.example.alex.droidcon2017_hackathon.core.GlobalVariables;
import com.example.alex.droidcon2017_hackathon.io.Dress;
import com.example.alex.droidcon2017_hackathon.io.DressSamples;

import org.w3c.dom.Text;

/**
 * Created by alex on 08/04/2017.
 */

public class ScanFragment extends Fragment {
    private static final String TAG = "scanFrag";

    private LinearLayout page0;
    private LinearLayout page1;

    private Dress dress = null;



    private String nfcID;
    private Button back;
    private ImageView image;
    private TextView color;
    private TextView size;
    private TextView tagList;
    private TextView name;
    private ImageButton colorSpeech;
    private ImageButton cabinet;
    private ImageButton favourite;
    private ImageButton assistant;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.scan_fragment, container, false);

        page0 = (LinearLayout) rootView.findViewById(R.id.scan_page_0);
        page1 = (LinearLayout) rootView.findViewById(R.id.scan_page_1);
        back = (Button) rootView.findViewById(R.id.scan_back_button);
        image = (ImageView) rootView.findViewById(R.id.scan_image);
        cabinet = (ImageButton) rootView.findViewById(R.id.scan_cabinet);
        favourite = (ImageButton) rootView.findViewById(R.id.scan_favourite);
        assistant = (ImageButton) rootView.findViewById(R.id.scan_assistent);
        colorSpeech = (ImageButton) rootView.findViewById(R.id.scan_color_audio_button);
        color = (TextView) rootView.findViewById(R.id.scan_color_label);
        size = (TextView) rootView.findViewById(R.id.scan_size_label);
        tagList = (TextView) rootView.findViewById(R.id.scan_tags_list);
        name = (TextView) rootView.findViewById(R.id.scan_name);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page1.setVisibility(View.GONE);
                page0.setVisibility(View.VISIBLE);
            }
        });
        colorSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).speak(color.getText().toString());
            }
        });

        cabinet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Core.getInstance().addTOWardrobe(dress)){
                    Intent notify = new Intent(GlobalVariables.NEW_ITEM_READY);
                    notify.putExtra("sender", "wardrobe");
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(notify);
                    Intent broadcast = new Intent(GlobalVariables.GOTO_CABINET);
                    broadcast.putExtra("nfcID", nfcID);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(broadcast);
                } else {
                    Toast.makeText(getContext(), "You already own this dress!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Core.getInstance().addToWishList(dress)){
                    Intent notify = new Intent(GlobalVariables.NEW_ITEM_READY);
                    notify.putExtra("sender", "wishlist");
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(notify);
                    Intent broadcast = new Intent(GlobalVariables.GOTO_FAVOURITE);
                    broadcast.putExtra("nfcID", nfcID);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(broadcast);
                } else {
                    Toast.makeText(getContext(), "You already added this dress to your wishlist!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        assistant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Core.getInstance().setTmpDress(dress);
                Intent broadcast = new Intent(GlobalVariables.GOTO_ASSISTANT);
                broadcast.putExtra("nfcID", nfcID);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(broadcast);
            }
        });

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter(GlobalVariables.NFC_READY));

        return rootView;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(GlobalVariables.NFC_READY)) {
                nfcID = intent.getExtras().getString("nfcID");
                Log.d(TAG, "Intent received: " + nfcID);
                if(page0.getVisibility()== View.VISIBLE) {
                    //TODO GetItem and show page
                    page0.setVisibility(View.GONE);
                    page1.setVisibility(View.VISIBLE);
                    if(DressSamples.DRESS_DATABASE.containsKey(nfcID)) {
                        dress = DressSamples.DRESS_DATABASE.get(nfcID);
                        fillViews(DressSamples.DRESS_DATABASE.get(nfcID));
                    }else{
                        dress = Core.getInstance().getWardrobe().get(nfcID);
                        fillViews(Core.getInstance().getWardrobe().get(nfcID));
                    }
                }
            }
        }

    };

    public void fillViews(Dress dress){
        //TODO
        Log.d(TAG, "Displaying stuff of " +  dress.getId());
        image.setImageURI(Uri.parse(dress.getUrlImage()));
        color.setText(dress.getMainColor());
        name.setText(dress.getName());
        size.setText("Size: " + dress.getTaglia());
        tagList.setText("");
        for(String s : dress.getTags()){
            tagList.setText(tagList.getText() + ", ");
        }
        tagList.setText(tagList.getText().subSequence(0, tagList.getText().length()-3)); //ROITO, ma fasto
        Core.getInstance().setTmpDress(dress);
    }
}
