package com.example.alex.droidcon2017_hackathon.GUI.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.widget.Toast;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.IntentFilter;

import com.example.alex.droidcon2017_hackathon.GUI.adapter.MenuAdapter;
import com.example.alex.droidcon2017_hackathon.R;
import com.example.alex.droidcon2017_hackathon.core.Core;
import com.example.alex.droidcon2017_hackathon.core.GlobalVariables;

import java.util.Locale;

public class MainActivity extends FragmentActivity {
    public static final String TAG = "src.gui.activity.main";

    private ViewPager mPager;
    private TabLayout tabLayout;
    private MenuAdapter adapter;

    private TextToSpeech tts;


    // list of NFC technologies detected:
    private final String[][] techList=new String[][] {
            new String[] {
                    NfcA.class.getName(),
                    NfcB.class.getName(),
                    NfcF.class.getName(),
                    NfcV.class.getName(),
                    IsoDep.class.getName(),
                    MifareClassic.class.getName(),
                    MifareUltralight.class.getName(),Ndef.class.getName()} };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Core.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPager = (ViewPager) findViewById(R.id.main_viewpager);
        adapter = new MenuAdapter(getSupportFragmentManager());
        mPager.setAdapter(adapter);
        tabLayout = (TabLayout) findViewById(R.id.main_tablayout);
        tabLayout.setupWithViewPager(mPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_tap_and_play_black_48dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_format_list_bulleted_black_48dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_insert_comment_black_48dp);

        NfcAdapter nfcAdapter=NfcAdapter.getDefaultAdapter(this);
        if(nfcAdapter==null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this,"This device doesn't support NFC.",Toast.LENGTH_LONG).show();
            finish();
            return; }
        if(!nfcAdapter.isEnabled()) Toast.makeText(this,"NFC disabled.",Toast.LENGTH_LONG).show();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(GlobalVariables.GOTO_CABINET));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(GlobalVariables.GOTO_FAVOURITE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(GlobalVariables.GOTO_ASSISTANT));


        //Text to speach initialization
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    result = tts.setSpeechRate(0.7f);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });
    }

    public void speak(String text){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        }else{
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // creating pending intent:
        PendingIntent pendingIntent=PendingIntent.getActivity(this, 0,new Intent(this,getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        // creating intent receiver for NFC events:
        IntentFilter filter=new IntentFilter();
        filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        // enabling foreground dispatch for getting intent from NFC event:
        NfcAdapter nfcAdapter=NfcAdapter.getDefaultAdapter(this);
        nfcAdapter.enableForegroundDispatch(this,pendingIntent,new IntentFilter[]{filter},this.techList); }

    @Override
    protected void onPause() {
        super.onPause();
        // disabling foreground dispatch:
        NfcAdapter nfcAdapter=NfcAdapter.getDefaultAdapter(this);
        nfcAdapter.disableForegroundDispatch(this); }

    @Override
    protected void onNewIntent(Intent intent) {
        if(intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
            //if(we are not in the NFC Scan fragment) return;
            byte[] nfcId=intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
            String nfcIdString=byteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
            /*PUT YOUR CODE HERE*/
            Log.d(TAG, "Received: " + nfcIdString);
            Intent broadcast = new Intent(GlobalVariables.NFC_READY);
            broadcast.putExtra("nfcID", nfcIdString);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcast);

        } }

    private String byteArrayToHexString(byte[] inArray) {
        int i,j,in;
        String[] hex={"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        String out="";
        for(j=0; j<inArray.length; ++j) {
            in=(int)inArray[j]&0xff;
            i=(in>>4)&0x0f;
            out+=hex[i];
            i=in&0x0f;
            out+=hex[i]; }
        return out; }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(GlobalVariables.GOTO_ASSISTANT)) {
                mPager.setCurrentItem(2);
            }else if (intent.getAction().equals(GlobalVariables.GOTO_CABINET)) {
                mPager.setCurrentItem(1);
            }else if (intent.getAction().equals(GlobalVariables.GOTO_FAVOURITE)) {
                mPager.setCurrentItem(1);
            }
        }

    };

    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

}
